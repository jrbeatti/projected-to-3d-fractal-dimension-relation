# README #

This is the R code for fitting and constructing the empirical relation between the projected and 3D fractal dimension, found in the paper:

Part I: The relation between the true and observed fractaldimensions of turbulent clouds
James R. Beattie, Christoph Federrath, Ralf S. Klessen