## Constructing the Empirical Relation Between 2D Projections and 3D Fractal Dimension in Turbulent Clouds
# Author: James Beattie
# Date: Friday 23rd, November.
library(tidyverse)
library(reshape2)
library(magrittr)
library(broom)


#setwd(set working directory)

########################################################################################################################
# Declarations
########################################################################################################################

# Define Mach numbers
Mach1   <- 1
Mach4   <- 4
Mach10  <- 10
Mach20  <- 20
Mach40  <- 40
Mach100 <- 100

########################################################################################################################
# Functions
########################################################################################################################

ExponentLabel <- function(l) { 
     l <- paste('10^',l) 
     l <- gsub(" ","", l)
     l <- gsub("e","10^", l) 
     parse(text=l) 
}

trunc <- function(x, ..., prec = 0) base::trunc(x * 10^prec, ...) / 10^prec

TimeTransform <- function(data,M){
     # Match the turnover time data across simlations
     
     uniqueTT <- seq(2.1,9.0,0.1)
     
     data <- mutate(data,TurnoverTime = trunc(TimeStep*M*2,prec=2))
     data <- dplyr::filter(data, TurnoverTime %in%  uniqueTT) 
     
     return(data)
}


reader_fun <- function(name1,name2,Mach,M){
     # inputs: 
     #
     # name1: projection dataset
     # name2: slice dataset
     # Mach: Mach number
     #
     # outputs:
     #
     # dataset ready to be merged. 
     #
     #integral scale
     if(Mach == "Mach 4"){
          L <- 2512
          
          # Subset the projection data by the x projection
          data1 <- read_csv(name1) 
          data1 <- TimeTransform(data1,M)
          # Work out which projection is which.
          for(i in 1:nrow(data1)){
               if(data1$Filename[i] %>% grepl("coldens_x",.,fixed=TRUE) == TRUE){
                    data1$Projection[i] <- 'x' 
               } else if(data1$Filename[i] %>% grepl("coldens_y",.,fixed=TRUE) == TRUE){
                    data1$Projection[i] <- 'y'           
               } else if(data1$Filename[i] %>% grepl("coldens_z",.,fixed=TRUE) == TRUE){
                    data1$Projection[i] <- 'z'   
               }
          }
          
          data1 <- dplyr::filter(data1,Projection == "x") %>%
               group_by(Length) %>%
               summarise(FD_Proj = mean(FractalDim),sd_FD_Proj = sd(FractalDim), 
                         mass_Proj = mean(Mass), sd_mass_Proj = sd(Mass)) %>%
               mutate(Length_trans = Length/L, Trans_mass_Proj = mass_Proj/max(mass_Proj),
                      sd_Trans_mass_Proj = sd_mass_Proj/max(mass_Proj)) %>%
               data.frame()
          
     }
     else{
          L <- 1024
          
          data1 <- read_csv(name1) %>%
               TimeTransform(.,M) %>%
               group_by(Length) %>%
               summarise(FD_Proj = mean(FractalDim),sd_FD_Proj = sd(FractalDim), 
                         mass_Proj = mean(Mass), sd_mass_Proj = sd(Mass)) %>%
               mutate(Length_trans = Length/L,Trans_mass_Proj = mass_Proj/max(mass_Proj),
                      sd_Trans_mass_Proj = sd_mass_Proj/max(mass_Proj)) %>%
               data.frame()   
          
          
     }
     

     data2 <- read_csv(name2) %>%
          TimeTransform(.,M) %>%
          group_by(Length) %>%
          summarise(FD_Slice = mean(FractalDim),sd_FD_Slice = sd(FractalDim), 
                    mass_Slice = mean(Mass), sd_mass_Slice = sd(Mass)) %>%
          mutate(Length_trans = Length/L,Trans_mass_Slice = mass_Slice/max(mass_Slice),
                 sd_Trans_mass_Slice = sd_mass_Slice/max(mass_Slice)) %>%
          data.frame()
     
     data1$FD_Slice                <- data2$FD_Slice
     data1$sd_FD_Slice             <- data2$sd_FD_Slice
     data1$sd_mass_Slice           <- data2$sd_mass_Slice
     data1$mass_Slice              <- data2$mass_Slice
     data1$Trans_mass_Slice        <- data2$Trans_mass_Slice
     data1$sd_Trans_mass_Slice     <- data2$sd_Trans_mass_Slice 
     
     Mach <- rep(Mach,nrow(data1))
     
     data1$Mach <- Mach
     
     return(data1)
}


reader_fun_composite <- function(name1,name2,Mach,M,lengthscale){
     # Parse the Mach 4 data
     if(Mach == "Mach 4"){
          
          #integral scale
          L <- 2512
          
          # Subset the projection data by the x projection
          data1 <- read_csv(name1) 
          data1 <- TimeTransform(data1,M)
          
          # Work out which projection is which.
          for(i in 1:nrow(data1)){
               if(data1$Filename[i] %>% grepl("coldens_x",.,fixed=TRUE) == TRUE){
                    data1$Projection[i] <- 'x' 
               } else if(data1$Filename[i] %>% grepl("coldens_y",.,fixed=TRUE) == TRUE){
                    data1$Projection[i] <- 'y'           
               } else if(data1$Filename[i] %>% grepl("coldens_z",.,fixed=TRUE) == TRUE){
                    data1$Projection[i] <- 'z'   
               }
          }
          
          data1 <- dplyr::filter(data1,Projection == "x") %>%
               group_by(Length) %>%
               summarise(FD_Proj = mean(FractalDim),sd_FD_Proj = sd(FractalDim), 
                         mass_Proj = mean(Mass), sd_mass_Proj = sd(Mass)) %>%
               mutate(Length_trans = log10(Length/L),
                      Fitting = 'Not Fitting') %>%
               data.frame()
          
          data2 <- read_csv(name2) %>%
               TimeTransform(.,M) %>%
               group_by(Length) %>%
               summarise(FD_Slice = mean(FractalDim),sd_FD_Slice = sd(FractalDim), 
                         mass_Slice = mean(Mass), sd_mass_Slice = sd(Mass)) %>%
               mutate(Length_trans = log10(Length/L),
                      Fitting = 'Not Fitting') %>%
               data.frame()   
          
          
     }
     # This is for the Mach 1 data: translate it by the sonic scale.
     else if(Mach == "Mach 1"){
          
          # Integral scale of the Mach 1 simulation
          L <- 1024
          
          # Sonic scale in the Mach 4 simulation
          SonicScale     <- 120.9765
          
          # Intergral scale of the Mach 4 simulation
          IntegralM4     <- 10048
          
          translation <- log10(L/(2*IntegralM4)) - log10(SonicScale/IntegralM4)
          
          data1 <- read_csv(name1) %>%
               TimeTransform(.,M) %>%
               group_by(Length) %>%
               summarise(FD_Proj = mean(FractalDim),sd_FD_Proj = sd(FractalDim), 
                         mass_Proj = mean(Mass), sd_mass_Proj = sd(Mass)) %>%
               mutate(Length_trans = log10(Length/IntegralM4) - translation,
                      Fitting = 'Not Fitting') %>%
               data.frame()
               
          data2 <- read_csv(name2) %>%
               TimeTransform(.,M) %>%
               group_by(Length) %>%
               summarise(FD_Slice = mean(FractalDim),sd_FD_Slice = sd(FractalDim), 
                         mass_Slice = mean(Mass), sd_mass_Slice = sd(Mass)) %>%
               mutate(Length_trans = log10(Length/IntegralM4) - translation,
                      Fitting = 'Not Fitting') %>%
               data.frame()      
          
               
     }
     # For all other data, scale it by the lengthscale argument.
     else{
          
          L <- 1024
          data1 <- read_csv(name1) %>%
               TimeTransform(.,M) %>%
               group_by(Length) %>%
               summarise(FD_Proj = mean(FractalDim),sd_FD_Proj = sd(FractalDim), 
                         mass_Proj = mean(Mass), sd_mass_Proj = sd(Mass)) %>%
               mutate(Length_trans = log10( (Length/L)*lengthscale ),
                      Fitting = 'Not Fitting') %>%
               data.frame()   
     
          data2 <- read_csv(name2) %>%
               TimeTransform(.,M) %>%
               group_by(Length) %>%
               summarise(FD_Slice = mean(FractalDim),sd_FD_Slice = sd(FractalDim), 
                         mass_Slice = mean(Mass), sd_mass_Slice = sd(Mass)) %>%
               mutate(Length_trans = log10( (Length/L)*lengthscale ),
                      Fitting = 'Not Fitting') %>%
               data.frame()       
               
     }

     
     data1$FD_Slice      <- data2$FD_Slice
     data1$sd_FD_Slice   <- data2$sd_FD_Slice
     data1$sd_mass_Slice <- data2$sd_mass_Slice
     data1$mass_Slice    <- data2$mass_Slice
     
     Mach <- rep(Mach,nrow(data1))
     
     data1$Mach <- Mach
     
     return(data1)
}


FittingBuilder<-function(data,lower,upper){
     for(i in 1:nrow(data)){
          if(data$Length[i] > lower & data$Length[i] < upper){
               data$Fitting[i] <- 'Fitting'
          }
     }
     return(data)
}


########################################################################################################################
# Model Functions
########################################################################################################################

# Error function
erf <- function(x) 2 * pnorm(x * sqrt(2)) - 1


# Model
nlsFunc2 <- function(x,parms){
     beta0 <- parms[1]
     beta1 <- parms[2]
     fmax <- parms[3]
     fmin <- parms[4]
     
     f <- (fmax-fmin)*(1-erf(beta1*x+beta0)) / 2 + fmin
     
     return(f)
}


########################################################################################################################
# Individual Fractal Dimension Curves
########################################################################################################################

1/log(10)


M1        <- reader_fun("MaxPixelMach1_1024_MassLength_Maxpixel_N1_proj.csv",
                             "MaxPixelMach1_1024_MassLength_Maxpixel_N1_slice.csv","Mach 1",Mach1)
M4        <- reader_fun("MaxPixelMach4_2512_MassLength_Maxpixel_N1_proj.csv",
                        "MaxPixelMach4_2512_MassLength_Maxpixel_N1_slice.csv","Mach 4",Mach4)
M10       <- reader_fun("MaxPixelMach10_1024_MassLength_Maxpixel_N1_proj.csv",
                        "MaxPixelMach10_1024_MassLength_Maxpixel_N1_slice.csv","Mach 10",Mach10)
M20       <- reader_fun("MaxPixelMach20_1024_MassLength_Maxpixel_N1_proj.csv",
                        "MaxPixelMach20_1024_MassLength_Maxpixel_N1_slice.csv","Mach 20",Mach20)
M40       <- reader_fun("MaxPixelMach40_1024_MassLength_Maxpixel_N1_proj.csv",
                        "MaxPixelMach40_1024_MassLength_Maxpixel_N1_slice.csv","Mach 40",Mach40)
M100      <- reader_fun("MaxPixelMach100_1024_MassLength_Maxpixel_N1_proj.csv",
                        "MaxPixelMach100_1024_MassLength_Maxpixel_N1_slice.csv","Mach 100",Mach100)


AllData <- rbind(M1,M4,M10,M20,M40,M100)
AllData %<>% mutate(Mach = factor(Mach, levels=c("Mach 1","Mach 4","Mach 10", "Mach 20","Mach 40","Mach 100")) )


AllData %>% ggplot() +
     geom_point(aes(x=log10(Length_trans),y=log10(Trans_mass_Proj)),col='red',shape=3) +
     geom_ribbon(aes(x=log10(Length_trans),
                     ymax=log10(Trans_mass_Slice) + (1/log(10))*(sd_Trans_mass_Slice/Trans_mass_Slice),
                     ymin=log10(Trans_mass_Slice) - (1/log(10))*(sd_Trans_mass_Slice/Trans_mass_Slice)),
                 alpha=0.2,fill='black') +
     geom_ribbon(aes(x=log10(Length_trans),
                     ymax=log10(Trans_mass_Proj) + (1/log(10))*(sd_Trans_mass_Proj/Trans_mass_Proj) ,
                     ymin=log10(Trans_mass_Proj) - (1/log(10))*(sd_Trans_mass_Proj/Trans_mass_Proj) ),
                 alpha=0.2,fill='red') +
     geom_point(aes(x=log10(Length_trans),y=log10(Trans_mass_Slice)))+
     facet_wrap(~Mach) +
     labs(x='log(Length)',y='log(Mass)') +
     scale_x_continuous(labels=ExponentLabel,
                        breaks = round(seq(-3, 1, by = 0.5),1)) + 
     scale_y_continuous(labels=ExponentLabel) + 
     theme_bw() +
     theme(axis.title = element_text(size = 16), 
           axis.text = element_text(size = 8), 
           axis.text.x = element_text(size = 12), 
           axis.text.y = element_text(size = 12), 
           plot.title = element_text(size = 20)) +
     theme(strip.text.x = element_text(size = 14))

AllData %>% ggplot() +
     geom_point(aes(x=log10(Length_trans),y=FD_Slice),col='black')+
     geom_ribbon(aes(x=log10(Length_trans),ymax=FD_Slice+sd_FD_Slice,ymin=FD_Slice-sd_FD_Slice),alpha=0.2,fill='black') +
     geom_point(aes(x=log10(Length_trans),y=FD_Proj),col='red',shape=3) +
     geom_ribbon(aes(x=log10(Length_trans),ymax=FD_Proj+sd_FD_Proj,ymin=FD_Proj-sd_FD_Proj),alpha=0.2,fill='red') +
     facet_wrap(~Mach) +
     scale_x_continuous(labels=ExponentLabel,
                        breaks = round(seq(-3, 1, by = 0.5),1)) + 
     labs(x='log(Length)',y='Fractal Dimension') +
     theme_bw() +
     theme(axis.title = element_text(size = 16), 
           axis.text = element_text(size = 8), 
           axis.text.x = element_text(size = 12), 
           axis.text.y = element_text(size = 12), 
           plot.title = element_text(size = 20)) +
     theme(strip.text.x = element_text(size = 14))

head(AllData)
AllData %>% ggplot(aes(x=FD_Proj,y=FD_Slice)) +
     geom_point(aes(col=Mach))+
     theme_bw() +
     labs(x='Projection Fractal Dimension',y='Slice Fractal Dimension') +
     theme(axis.title = element_text(size = 16), 
           axis.text = element_text(size = 8), 
           axis.text.x = element_text(size = 12), 
           axis.text.y = element_text(size = 12), 
           plot.title = element_text(size = 20)) +
     theme(strip.text.x = element_text(size = 14),
           legend.position = c(0.1,0.8),
           legend.text = element_text(size=12),
           legend.title = element_text(size=14))+
     scale_color_brewer(type = 'div', palette = 'Set1', direction = 1)



########################################################################################################################
# Stitch Curves Together
########################################################################################################################

# Scale Transforms
l_1 <- (Mach1/Mach4)^2
l_10 <- (Mach10/Mach4)^2
l_20 <- (Mach20/Mach4)^2
l_40 <- (Mach40/Mach4)^2
l_100 <- (Mach100/Mach4)^2

# Building the composite datasets
M1_comp        <- reader_fun_composite("MaxPixelMach1_1024_MassLength_Maxpixel_N1_proj.csv",
                        "MaxPixelMach1_1024_MassLength_Maxpixel_N1_slice.csv","Mach 1",Mach1,l_1)
M4_comp        <- reader_fun_composite("MaxPixelMach4_2512_MassLength_Maxpixel_N1_proj.csv",
                        "MaxPixelMach4_2512_MassLength_Maxpixel_N1_slice.csv","Mach 4",Mach4,1)
M10_comp       <- reader_fun_composite("MaxPixelMach10_1024_MassLength_Maxpixel_N1_proj.csv",
                        "MaxPixelMach10_1024_MassLength_Maxpixel_N1_slice.csv","Mach 10",Mach10,l_10)
M20_comp       <- reader_fun_composite("MaxPixelMach20_1024_MassLength_Maxpixel_N1_proj.csv",
                        "MaxPixelMach20_1024_MassLength_Maxpixel_N1_slice.csv","Mach 20",Mach20,l_20)
M40_comp       <- reader_fun_composite("MaxPixelMach40_1024_MassLength_Maxpixel_N1_proj.csv",
                        "MaxPixelMach40_1024_MassLength_Maxpixel_N1_slice.csv","Mach 40",Mach40,l_40)
M100_comp      <- reader_fun_composite("MaxPixelMach100_1024_MassLength_Maxpixel_N1_proj.csv",
                        "MaxPixelMach100_1024_MassLength_Maxpixel_N1_slice.csv","Mach 100",Mach100,l_100)

# Building the fitting regions
M1_comp        <- FittingBuilder(M1_comp,30,max(M1_comp$Length)/2)
M4_comp        <- FittingBuilder(M4_comp,7.5,max(M4_comp$Length)/2)
M10_comp       <- FittingBuilder(M10_comp,30,max(M10_comp$Length)/2)
M20_comp       <- FittingBuilder(M20_comp,30,max(M20_comp$Length)/2)
M40_comp       <- FittingBuilder(M40_comp,30,max(M40_comp$Length)/2)
M100_comp      <- FittingBuilder(M100_comp,30,max(M100_comp$Length)/2)

Comp_data <- rbind(M1_comp,M4_comp,M10_comp,M20_comp,M40_comp,M100_comp)
Comp_data %<>% mutate(Mach = factor(Mach, levels=c("Mach 1","Mach 4","Mach 10", "Mach 20","Mach 40","Mach 100")) )

Comp_data_fit       <- filter(Comp_data,Fitting=="Fitting")
Comp_data_nofit     <- filter(Comp_data,Fitting=="Not Fitting")


# Construction of the model
Comp_data_fit %>% head()

fmax_proj      <- 2
fmax_slice     <- 3
formulaExp_Proj     <- as.formula(FD_Proj  ~ ( (fmax_proj-fmin)*(1-erf(beta1*Length_trans+beta0)) / 2 + fmin ) )
formulaExp_Slice    <- as.formula( (FD_Slice+1)  ~ ( (fmax_slice-fmin)*(1-erf(beta1*Length_trans+beta0)) / 2 + fmin ) )

# Nonlinear Least Squares
nls_Proj       <- nls(formulaExp_Proj, start = list(beta0 = 0,beta1 = 1/2,fmin=1.4),data = Comp_data_fit,weights=sd_FD_Proj)
nls_Slice      <- nls(formulaExp_Slice, start = list(beta0 = 0,beta1 = 1/2,fmin=2),data = Comp_data_fit,weights=sd_FD_Slice)

ProjParms      <- tidy(nls_Proj)
SliceParms     <- tidy(nls_Slice)

ProjParms <- c(ProjParms$estimate[1],ProjParms$estimate[2],fmax_proj,ProjParms$estimate[3])
SliceParms <- c(SliceParms$estimate[1],SliceParms$estimate[2],fmax_slice,SliceParms$estimate[3])

# Construction of the Error for both models

## Projections

# Lower Temporal Fluctuations
PLower <- dplyr::select(Comp_data_fit,Length_trans,FD_Proj,sd_FD_Proj) %>%
     mutate(Lower = FD_Proj-sd_FD_Proj)

# Model
formulaExp_Proj_L     <- as.formula( Lower ~ ( (fmax_proj-fmin)*(1-erf(beta1*Length_trans+beta0)) / 2 + fmin ) )
nls_Proj_L            <- nls(formulaExp_Proj_L, start = list(beta0 = 0,beta1 = 1/2,fmin=1.4),data = PLower,weights=sd_FD_Proj)
ProjParms_L    <- tidy(nls_Proj_L)
ProjParms_L    <- c(ProjParms_L$estimate[1],ProjParms_L$estimate[2],fmax_proj,ProjParms_L$estimate[3])

# Upper Temporal Fluctuations
PUpper <- dplyr::select(Comp_data_fit,Length_trans,FD_Proj,sd_FD_Proj) %>%
     mutate(Upper = FD_Proj+sd_FD_Proj)

# Model
formulaExp_Proj_U     <- as.formula( Upper  ~ ( (fmax_proj-fmin)*(1-erf(beta1*Length_trans+beta0)) / 2 + fmin ) )
nls_Proj_U            <- nls(formulaExp_Proj_U, start = list(beta0 = 0,beta1 = 1/2,fmin=1.4),data = PUpper,weights=sd_FD_Proj)
ProjParms_U    <- tidy(nls_Proj_U)
ProjParms_U    <- c(ProjParms_U$estimate[1],ProjParms_U$estimate[2],fmax_proj,ProjParms_U$estimate[3])
## Slices

# Lower Temporal Fluctuations
SLower <- dplyr::select(Comp_data_fit,Length_trans,FD_Slice,sd_FD_Slice) %>%
     mutate(Lower = (FD_Slice+1)-sd_FD_Slice)

# Model
formulaExp_Slice_L    <- as.formula( Lower  ~ ( (fmax_slice-fmin)*(1-erf(beta1*Length_trans+beta0)) / 2 + fmin ) )
nls_Slice_L      <- nls(formulaExp_Slice_L, start = list(beta0 = 0,beta1 = 1/2,fmin=2),data = SLower,weights=sd_FD_Slice)
SliceParms_L     <- tidy(nls_Slice_L)
SliceParms_L <- c(SliceParms_L$estimate[1],SliceParms_L$estimate[2],fmax_slice,SliceParms_L$estimate[3])

# Upper Temporal Fluctuations
SUpper <- dplyr::select(Comp_data_fit,Length_trans,FD_Slice,sd_FD_Slice) %>%
     mutate(Upper = (FD_Slice+1)+sd_FD_Slice)

# Model
formulaExp_Slice_U    <- as.formula( Upper ~ ( (fmax_slice-fmin)*(1-erf(beta1*Length_trans+beta0)) / 2 + fmin ) )
nls_Slice_U      <- nls(formulaExp_Slice_U, start = list(beta0 = 0,beta1 = 1/2,fmin=2),data = SUpper,weights=sd_FD_Slice)
SliceParms_U     <- tidy(nls_Slice_U)
SliceParms_U <- c(SliceParms_U$estimate[1],SliceParms_U$estimate[2],fmax_slice,SliceParms_U$estimate[3])


png("Figure4_CompositeCurves.png", width = 2000, height = 1200, units = 'px', res = 200)
ggplot(aes(x=Length_trans),data=Comp_data_fit)+
     geom_point(data=Comp_data_fit,aes(x=Length_trans,y=FD_Proj,col=Mach)) +
     geom_ribbon(data=Comp_data_fit,aes(x=Length_trans,ymin=FD_Proj-sd_FD_Proj,ymax=FD_Proj+sd_FD_Proj,fill=Mach),alpha=0.1)+
     geom_point(data=Comp_data_fit,aes(x=Length_trans,y=FD_Slice+1,col=Mach)) +
     geom_ribbon(data=Comp_data_fit,aes(x=Length_trans,ymin=FD_Slice-sd_FD_Slice+1,ymax=FD_Slice+sd_FD_Slice+1,fill=Mach),
                 alpha=0.1) +
     geom_point(data=Comp_data_nofit,aes(x=Length_trans,y=FD_Proj),col='grey80') +
     geom_point(data=Comp_data_nofit,aes(x=Length_trans,y=FD_Slice+1),col='grey80') +
     theme_bw() +
     scale_color_manual(values=c("#2121D9", "#9999FF", "#21D921", "#D92121", "#FF9326", "#CCCC00")) +
     scale_fill_manual(values=c("#2121D9", "#9999FF", "#21D921", "#D92121", "#FF9326", "#CCCC00")) +
     scale_y_continuous(breaks = round(seq(1.4,3.1, by = 0.1),1),limits=c(1.4,3.1)) + 
     scale_x_continuous(breaks = round(seq(min(M1_comp$Length_trans), max(M100_comp$Length_trans)+5, by = 0.5),1)-0.1,
                        labels=ExponentLabel,limits = c(-4,3)) + 
     theme(axis.title = element_text(size = 16), 
           axis.text = element_text(size = 8), 
           axis.text.x = element_text(size = 12), 
           axis.text.y = element_text(size = 12), 
           plot.title = element_text(size = 15)) + 
     labs(x = expression(paste(l/L)), 
          y = 'Fractal Dimension') + 
     theme(legend.text = element_text(size = 10), 
           legend.title = element_text(size = 12,face = "bold")) + 
     theme(legend.background = element_rect(fill = "white"), legend.position = c(0.9, 0.80)) +
     stat_function(fun=nlsFunc2,args=list(parms=ProjParms),col='black',size=1.5) +
     stat_function(fun=nlsFunc2,args=list(parms=SliceParms),col='black',size=1.5) +
     stat_function(fun=nlsFunc2,args=list(parms=ProjParms_U),col='black',linetype=2) +
     stat_function(fun=nlsFunc2,args=list(parms=SliceParms_L),col='black',linetype=2) +
     stat_function(fun=nlsFunc2,args=list(parms=ProjParms_L),col='black',linetype=2) +
     stat_function(fun=nlsFunc2,args=list(parms=SliceParms_U),col='black',linetype=2) +
     geom_segment(y = SliceParms[4], yend = SliceParms[4],x=1.5,xend=3.5,linetype=3) +
     geom_segment(y = ProjParms[4],yend = ProjParms[4],x=1.5,xend=3.5,linetype=3) +
     geom_segment(y = 2,yend = 2,x=-4.5,xend=-2.5,linetype=3) +
     geom_segment(y = 3,yend=3,x=-4.5,xend=-2.5,linetype=3) 
dev.off()


########################################################################################################################
# Building the Map Between 2D Projections and 3D FD
########################################################################################################################


LengthData <-data.frame(Length_trans = seq(-6,5,0.001))

ModelDat <- data.frame(FD_Proj = predict(nls_Proj,newdata=LengthData),
           FD_Proj_U = predict(nls_Proj_U,newdata=LengthData) - predict(nls_Proj,newdata=LengthData),
           FD_Proj_L = predict(nls_Proj_L,newdata=LengthData) - predict(nls_Proj,newdata=LengthData),
           FD_Slice = predict(nls_Slice,newdata=LengthData),
           FD_Slice_U = predict(nls_Slice_U,newdata=LengthData) - predict(nls_Slice,newdata=LengthData),
           FD_Slice_L = predict(nls_Slice_L,newdata=LengthData) - predict(nls_Slice,newdata=LengthData),
           Length_trans = seq(-6,5,0.001)) %>%
     mutate(Upper = sqrt(FD_Proj_L^2 + FD_Slice_L^2), Lower = sqrt(FD_Slice_U^2 + FD_Proj_U^2),
            Theory1 = FD_Proj+1,
            Theory2 = FD_Proj+1/2)

head(ModelDat)
ggplot(aes(x=FD_Proj,y=FD_Slice),data=ModelDat) +
     geom_line(size=2) +
     geom_ribbon(aes(x=FD_Proj,ymin=FD_Slice-Lower,ymax=FD_Slice+Upper),alpha=0.1) + 
     geom_abline(slope=1,intercept = 1/2,col='blue',linetype=2) +
     geom_abline(slope=1,intercept = 1,col='red',linetype=2) +
     theme_bw() +
     theme(axis.title = element_text(size = 16), 
           axis.text = element_text(size = 8), 
           axis.text.x = element_text(size = 12), 
           axis.text.y = element_text(size = 12), 
           plot.title = element_text(size = 15)) 

ModelDat %<>% mutate(Diff = FD_Proj - FD_Slice)

ggplot() +
     geom_line(aes(x=Length_trans,y=abs(Diff)),data=ModelDat) +
     scale_x_continuous(breaks = round(seq(-6, 5, by = 1),1),
                        labels=ExponentLabel) +
     ylim(0.45,1) +
     theme_bw() +
     theme(axis.title = element_text(size = 16), 
           axis.text = element_text(size = 8), 
           axis.text.x = element_text(size = 12), 
           axis.text.y = element_text(size = 12), 
           plot.title = element_text(size = 15)) +
     geom_hline(yintercept = 0.5,col='blue') +
     geom_hline(yintercept = 1,col='red')
     

